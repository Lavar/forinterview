package com.mycompany.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Samsung
 */
import com.mycompany.file.FileProcess;
import com.mycompany.file.HashToFile;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FileUtils;

import org.primefaces.event.FileUploadEvent;

@Named
@SessionScoped
public class FileUploadView implements Serializable {

    @EJB
    FileProcess fileProcess;
    @EJB
    HashToFile hashToFile;
    private List<Map<String, Integer>> text = new ArrayList<>();
    private List<File> files = new ArrayList<>();
    private Map<String, Integer> fromAtoG = new HashMap<>();
    private Map<String, Integer> fromHtoN = new HashMap<>();
    private Map<String, Integer> fromOtoU = new HashMap<>();
    private Map<String, Integer> fromVtoZ = new HashMap<>();

    public Map<String, Integer> getFromAtoG() {
        return fromAtoG;
    }

    public void setFromAtoG(Map<String, Integer> fromAtoG) {
        this.fromAtoG = fromAtoG;
    }

    public Map<String, Integer> getFromHtoN() {
        return fromHtoN;
    }

    public void setFromHtoN(Map<String, Integer> fromHtoN) {
        this.fromHtoN = fromHtoN;
    }

    public Map<String, Integer> getFromOtoU() {
        return fromOtoU;
    }

    public void setFromOtoU(Map<String, Integer> fromOtoU) {
        this.fromOtoU = fromOtoU;
    }

    public Map<String, Integer> getFromVtoZ() {
        return fromVtoZ;
    }

    public void setFromVtoZ(Map<String, Integer> fromVtoZ) {
        this.fromVtoZ = fromVtoZ;
    }

    public List<Map<String, Integer>> getText() {
        return text;
    }

    public void setText(List<Map<String, Integer>> text) {
        this.text = text;
    }

    public void handleFileUpload(FileUploadEvent event) throws IOException {
        //I need it because I am using Session scope 
        //(I would never use Session scope in real application for such case)
        fromAtoG = new HashMap<>();
        fromHtoN = new HashMap<>();
        fromOtoU = new HashMap<>();
        fromVtoZ = new HashMap<>();
        
        File destFile = new File(event.getFile().getFileName());
        FileUtils.copyInputStreamToFile(event.getFile().getInputstream(), destFile);
        files.add(destFile);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String results() {
        if (files.isEmpty()) {
            FacesMessage message = new FacesMessage("Please, upload files!");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }
        ArrayList<FileProcess> threadlist = new ArrayList<>();
        files.stream().map((file) -> new FileProcess(file)).map((t) -> {
            threadlist.add(t);
            return t;
        }).forEach((t) -> {
            t.start();
        });

        threadlist.stream().forEach((t) -> {
            try {
                t.join();
                text.add(t.getMap());
            } catch (InterruptedException ex) {
                Logger.getLogger(FileUploadView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        text.stream().forEach((textF) -> {
            for (Map.Entry<String, Integer> entry : textF.entrySet()) {
                if (entry.getKey().matches("^[a-g].*$")) {
                    //I made redundant part on purpose in order increase readability
                    if (fromAtoG.containsKey(entry.getKey())) {
                        fromAtoG.put(entry.getKey(), fromAtoG.get(entry.getKey()) + entry.getValue());
                    } else {
                        fromAtoG.put(entry.getKey(), entry.getValue());
                    }
                }
                if (entry.getKey().matches("^[h-n].*$")) {
                    if (fromHtoN.containsKey(entry.getKey())) {
                        fromHtoN.put(entry.getKey(), fromHtoN.get(entry.getKey()) + entry.getValue());
                    } else {
                        fromHtoN.put(entry.getKey(), entry.getValue());
                    }
                }
                if (entry.getKey().matches("^[o-u].*$")) {
                    if (fromOtoU.containsKey(entry.getKey())) {
                        fromOtoU.put(entry.getKey(), fromOtoU.get(entry.getKey()) + entry.getValue());
                    } else {
                        fromOtoU.put(entry.getKey(), entry.getValue());
                    }
                }
                if (entry.getKey().matches("^[v-z].*$")) {
                    if (fromVtoZ.containsKey(entry.getKey())) {
                        fromVtoZ.put(entry.getKey(), fromVtoZ.get(entry.getKey()) + entry.getValue());
                    } else {
                        fromVtoZ.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        });              
        cleanData();
        //only because it is written in specification
        saveResultsInFiles();
        return null;
    }
    //only because it is written in specification
    private void saveResultsInFiles() {
        try {
            hashToFile.saveHashToFile(fromAtoG, "fromAtoG.txt");
            hashToFile.saveHashToFile(fromHtoN, "fromHtoN.txt");
            hashToFile.saveHashToFile(fromOtoU, "fromOtoU.txt");
            hashToFile.saveHashToFile(fromVtoZ, "fromVtoZ.txt");
        } catch (IOException ex) {
            Logger.getLogger(FileUploadView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cleanData() {
        //we need it, because I am using Session scope
        files.removeAll(files);
        text.removeAll(text);
    }
}
