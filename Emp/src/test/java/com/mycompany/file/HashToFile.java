/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.file;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;
import javax.ejb.Stateless;

/**
 *
 * @author Samsung
 */
@Stateless
public class HashToFile {

    public void saveHashToFile(Map<String, Integer> map, String fileName) throws FileNotFoundException, IOException {
        FileOutputStream fos
                = new FileOutputStream(fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
        fos.close();
    }
}
